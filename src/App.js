import './App.scss';
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useState } from 'react'
import Register from './components/Register';
import ApolloProvider from './ApolloProvider'
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from './components/Home';
import Login from './components/Login';



function App() {

 
  return (
    <ApolloProvider>
        <BrowserRouter>
                <Container>
                  <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route exact path='/register' component={Register}/>
                    <Route exact path='/login' component={Login}/>
                  </Switch>       
                </Container>
              </BrowserRouter>      
    </ApolloProvider>

  );
}

export default App;
