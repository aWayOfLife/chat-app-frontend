import { InMemoryCache, ReactiveVar, makeVar } from "@apollo/client";
import jwt_decode from "jwt-decode";

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        auth: {
          read () {
            return authVar();
          }
        }

      }
    }
  }
});

const token = localStorage.getItem('token')

const authInitialValue = {
    token: token || null,
    user: jwt_decode(token).user || null
}



export const authVar = makeVar(
  authInitialValue
)