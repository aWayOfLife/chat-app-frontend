import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useState } from 'react'
import {REGISTER_USER} from '../graphql/Mutations'
import {useMutation} from '@apollo/client'


function Register(props) {
    const [variables, setVariables] = useState({
        name:'',
        email:'',
        password:''
    })
    const [registerUser, {loading}] = useMutation(REGISTER_USER,{
        update(_, res){
            console.log(res)
            props.history.push('/login')
        },
        onError(err){
            console.log(err)
        }
    })
    const submitRegisterForm = (event) =>{
        event.preventDefault()
        console.log(variables)
        registerUser({variables:{
            userInput:variables
        }})
    }
    return (
        <div>
            <Row>
                <Col>
                <h1>Register</h1>
                <Form onSubmit={submitRegisterForm}>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" value={variables.name} onChange={e => setVariables({...variables, name: e.target.value })}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" value={variables.email} onChange={e => setVariables({...variables, email: e.target.value })}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="text" value={variables.password} onChange={e => setVariables({...variables, password: e.target.value })}/>
                    </Form.Group>    
                    <Button variant="success" type="submit">
                        Register
                    </Button>
                </Form>
                </Col>
        </Row>
        </div>
    )
}

export default Register
