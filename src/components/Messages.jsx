import {useQuery,useLazyQuery, gql, useSubscription} from '@apollo/client'
import {MESSAGES} from '../graphql/Queries'
import {useEffect, useState} from 'react'
import {MESSAGE_SUBSCRIPTION} from '../graphql/Subscriptions'

let unSubscribe = null

function Messages({activeConversation}) {

const [messagesQueryExecutor, { loading, refetch, data, error, subscribeToMore }] = useLazyQuery(MESSAGES);

    useEffect(() => {
        if (activeConversation && activeConversation!=null) {
            console.log('queryexecuted', activeConversation)
        refetch ? refetch({ conversationId:activeConversation }) : messagesQueryExecutor({ variables: { conversationId:activeConversation } });
        }
    }, [activeConversation, refetch]);




    // const {subscribeToMore, loading, data} = useQuery(MESSAGES,{
    //     variables:{
    //         conversationId:'608912966dce8a00e4bbed95'
    //     }
    // })
    useEffect(()=>{

            
            console.log('call to subscribe')
            if (activeConversation && subscribeToMore) {
                const updateUnsubscribe  = subscribeToMore({
                    document:MESSAGE_SUBSCRIPTION,
                    variables:{
                        userId:'6087bc35ff282b1a68fd2452'
                    },
                    updateQuery:(prev, {subscriptionData}) =>{
                        if(!subscriptionData.data) return prev
                        const message = subscriptionData.data.newMessage
                        if(message.conversation._id===activeConversation){
                            return{
                                messages: [...prev.messages, message]
                            }
                        }
                        
                        
                        
                    }
                })

                        return () => {
                            updateUnsubscribe();
                        };
        }


        
        
    }, [subscribeToMore,activeConversation])


    


    return (
        <div>
            {data?.messages?.map(message => <p key={message._id}>{message.text}</p>)}
        </div>
    )
}

export default Messages
