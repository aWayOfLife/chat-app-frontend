import {useLazyQuery,useQuery, gql} from '@apollo/client'
import {LOGIN_USER} from '../graphql/Queries'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useState } from 'react'
import { authVar } from '../ApolloCache'

function Login(props) {
    const [variables, setVariables] = useState({
        email:'',
        password:''
    })
    const [loginUser, {loading}] = useLazyQuery(LOGIN_USER,{
        onError(err){
            console.log(err)
        },
        onCompleted(data){
            console.log(data)
            let auth = {token:data.loginUser.token, user:null}
            authVar(auth)
            localStorage.setItem('token',auth.token)
            props.history.push('/')
        },
        fetchPolicy: 'network-only'
    })
    const submitLoginForm = (event) =>{
        event.preventDefault()
        console.log(variables)
        loginUser({variables})
    }
    return (
        <div>
            <Row>
                <Col>
                <h1>Login</h1>
                <Form onSubmit={submitLoginForm}>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" value={variables.email} onChange={e => setVariables({...variables, email: e.target.value })}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="text" value={variables.password} onChange={e => setVariables({...variables, password: e.target.value })}/>
                    </Form.Group>    
                    <Button variant="success" type="submit">
                        Login
                    </Button>
                </Form>
                </Col>
        </Row>
        </div>
    )
}

export default Login
