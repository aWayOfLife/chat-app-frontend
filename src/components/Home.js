import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {USERS, CONVERSATIONS, MESSAGES} from '../graphql/Queries'
import {Fragment, useEffect, useState} from 'react'
import {useLazyQuery,useQuery, gql, useSubscription} from '@apollo/client'
import {MESSAGE_SUBSCRIPTION} from '../graphql/Subscriptions'
import {GET_AUTH} from '../graphql/Queries'
import Conversations from './Conversations'
import Messages from './Messages'

function Home(props) {
    const [activeConversation, setActiveConversation] = useState(null)
    const [myQueryExecutor, { data }] = useLazyQuery(GET_AUTH,{
        onError(err){
            console.log(err)
        },
        onCompleted(data){
            console.log("comp")
            console.log(data.auth)
        },
        fetchPolicy: 'network-only'

    });

    useEffect(
        () => { 
            console.log("exe")

            myQueryExecutor(); 
        },
        [myQueryExecutor]
    );

    const selectConversation = (conversationId) =>{
        setActiveConversation(conversationId)
    }

    return (
        <Fragment>
            <Row>
                <Link to="/login">
                    <Button variant="link">Login</Button>
                </Link>
                <Link to="/register">
                    <Button variant="link">Register</Button>
                </Link>
                <Button variant="link">Logout</Button>
            </Row>
            <Row>
                <Col xs={4}>
                    <Conversations selectConversation={selectConversation}/>
                </Col>
                <Col xs={8}>
                    <Messages activeConversation={activeConversation}/>
                </Col>
            </Row>
        </Fragment>
        
    )
}

export default Home
