import {useQuery, gql, useSubscription} from '@apollo/client'
import {CONVERSATIONS} from '../graphql/Queries'
import {useEffect, useState} from 'react'
import {CONVERSATION_SUBSCRIPTION} from '../graphql/Subscriptions'
import SingleConversation from './SingleConversation'

let unSubscribe = null


function Conversations({selectConversation}) {

    const {subscribeToMore, loading, data} = useQuery(CONVERSATIONS)
    useEffect(()=>{
        if(!unSubscribe){
                unSubscribe = subscribeToMore({
                    document:CONVERSATION_SUBSCRIPTION,
                    variables:{
                        userId:'6087bc35ff282b1a68fd2452'
                    },
                    updateQuery:(prev, {subscriptionData}) =>{
                        if(!subscriptionData.data) return prev
                        const conversation = subscriptionData.data.newConversation
                        return{
                            conversations: [...prev.conversations, conversation]
                        }
                    }
                })
    }
    }, [])


    


    return (
        <div>
            {data?.conversations?.map(conversation => <SingleConversation key={conversation._id} conversationId={conversation._id} selectConversation={selectConversation}/>)}
        </div>
    )
}

export default Conversations
