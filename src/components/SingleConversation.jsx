function SingleConversation(props) {
    return (
        <div onClick={() => props.selectConversation(props.conversationId)}>
            {props.conversationId}
        </div>
    )
}

export default SingleConversation
