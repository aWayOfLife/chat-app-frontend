import {gql} from '@apollo/client'

export const MESSAGE_SUBSCRIPTION = gql`
  subscription newMessage($userId:String!){
    newMessage(userId:$userId) {
        text
        _id
        conversation{
          _id
        }
    }
    }
`;

export const CONVERSATION_SUBSCRIPTION = gql`
  subscription newConversation($userId:String!){
    newConversation(userId:$userId) {
        _id

    }
    }
`;
