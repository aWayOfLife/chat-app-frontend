import {gql} from '@apollo/client'

export const GET_AUTH = gql`
  query GetAuth{
    auth @client { 
      token
      user{
        _id
        name
        email
      }
    }
  }
`

export const LOGIN_USER = gql`
  query loginUser($email: String! $password: String!) {
    loginUser(email:$email password:$password) {
        token
    }
  },
`;

export const USERS = gql`
  query users{
      users{
        _id
        name
        email
    }  
  }
`;

export const CONVERSATIONS = gql`
  query conversations{
      conversations{
        _id
    }  
  }
`;

export const MESSAGES = gql`
  query messages($conversationId:String!){
      messages(conversationId:$conversationId){
        text
        _id
        conversation{
          _id
        }
    }
  }
  
`;

