import { gql, useMutation } from '@apollo/client';




export const REGISTER_USER = gql`
  mutation registerUser($userInput: UserInput) {
    registerUser(userInput: $userInput) {
      token
    }
  },
`;



